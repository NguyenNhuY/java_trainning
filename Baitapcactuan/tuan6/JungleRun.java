import java.io.*;
import java.util.*;
class Coordinate
{
	private int x,y;
	public void setXY(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	public int getX()
	{
		return x;
	}
	public int getY()
	{
		return y;
	}
	public boolean equals(Coordinate other)
	{
		if( this.x == other.x && this.y == other.y)
			return true;
		else
			return false;
	}
	public String toString()
	{
		return "(" + x + "," + y + ")";
	}
}
public class JungleRun
{
	int col[] = {0, -1, 0, 1};
	int row[] = {1, 0, -1, 0};
	int N;
	char Graph[][] ;
	Coordinate start = new Coordinate();
	Coordinate end = new Coordinate();
	public void readFileInput()
	{

		File file = new File("input2.txt");
		try(Scanner sc = new Scanner(file))
		{
			 N = sc.nextInt();
			Graph = new char[N][N];
			for(int i = 0 ; i < N; i++)
				for(int j = 0; j < N; j++)
				{
					Graph[i][j] = sc.next().charAt(0);
					if(Graph[i][j] == 'S')
					start.setXY(i,j);
					if(Graph[i][j] == 'E')
					end.setXY(i,j);
				}
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		} 
	}
	public void DFS() 
	{
		int kt = 0, done = 0;
		Coordinate c = new Coordinate();
		
		Stack<Coordinate> path = new Stack<>();
		Stack<Coordinate> listCell = new Stack<>();
		listCell.push(start);
		while(done!=1)
		{
			if(!listCell.empty())
			{
				c = listCell.pop();
		
				for(int k = 0; k < 4; k++)
				{
					int _x = c.getX() + col[k];
					int _y = c.getY() + row[k];
					if ( _x >= 0 && _x < N && _y >= 0 && _y < N && Graph[_x][_y] !='T' )
					{
						Coordinate tam = new Coordinate();
						tam.setXY(_x,_y);
						listCell.push(tam);
						Graph[_x][_y] = 'T';
						kt = 1;
					}
					
				}
				if(kt == 1)
				{
					path.push(c);
					kt = 0;
					if(c.equals(end))
					{
						done = 1;
					}
				}
			}
		}
		System.out.println(path);
	}
	public static void main(String[] args)
	{
		
		JungleRun jg = new JungleRun();
		
		jg.readFileInput();
		jg.DFS();
		
	}
} 